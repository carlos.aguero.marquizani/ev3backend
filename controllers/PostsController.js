const utils = require('../resources/utils')
const moment = require('moment')
const connection = require('../config/db')
const { response } = require('express')

const userPost = (request,response) => {
 
  const user_id = request.body.user_id

  connection.query(
    `SELECT * FROM usuarios 
     join documentos on (usuarios.id = documentos.id)
    where usuarios.id = ${user_id}`,
    function(err, result, fields) {
      if (err) {console.log(err)}
        response.json(result)
    }
  );
}

const postComment = (request,response) => {
  const user_id = request.body.user_id
  const tipo_doc = request.body.title
  const link_doc = request.body.content
  connection.query(
    `INSERT INTO documentos (id,tipo_doc,link_doc)
    VALUES(${user_id}, "${tipo_doc}", "${link_doc}");`,
    function(err) {
      if (err) {
        message = err
        response.json({state:false,message})
      }else{
        response.json({state:true,message:"Se insertó el comentario"})
      }
        
    }
  );
}


module.exports = {
  userPost,
  postComment,
}