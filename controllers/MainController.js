const utils = require('../resources/utils')
const moment = require('moment')

const getIndex = (request,response) =>{
    response.render('index');
}

const getSignUp = (req, res) => {
    res.render('signup');
}

var posts = [
    ]

const getPost = (request,response) =>{
    response.render('post');
}

const setPost = (request,response) =>{
    posts.push({
        nombre: request.body.nombre,
        comentario:request.body.content,
        email: request.body.email,
    })
    
    response.render('post');
}

module.exports = {
    getIndex,
    getSignUp,
    getPost,
    setPost,
    
}